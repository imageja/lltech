package javax.script;

/* This is a placeholder class to be able to compile ImageJA with JDK5 */

public interface ScriptEngine {
	public Object eval(String commands);
}
