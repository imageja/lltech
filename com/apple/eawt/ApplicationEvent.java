package com.apple.eawt;

/* This is a placeholder class for compiling only */

public class ApplicationEvent {
	public void setHandled(boolean flag) {
		throw new RuntimeException("I am not a Mac");
	}

	public String getFilename() {
		throw new RuntimeException("I am not a Mac");
	}
}
